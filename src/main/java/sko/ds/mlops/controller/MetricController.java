package sko.ds.mlops.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sko.ds.mlops.service.serviceImpl.MetricServiceImpl;

import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@RequestMapping("metric")
@RestController
public class MetricController {

    private final MetricServiceImpl metricService;

    @GetMapping("")
    public ResponseEntity<?> findById() {
        List<Object> list = metricService.readFile();
        return null;//ResponseEntity.ok();
    }
}
