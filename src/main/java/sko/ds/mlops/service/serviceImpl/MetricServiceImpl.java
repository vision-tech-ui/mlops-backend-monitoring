package sko.ds.mlops.service.serviceImpl;

import lombok.RequiredArgsConstructor;
import org.hibernate.mapping.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sko.ds.mlops.dao.MetricMapper;
import sko.ds.mlops.domain.MetricDto;
import sko.ds.mlops.service.MetricService;

import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@RequiredArgsConstructor
@Service
public class MetricServiceImpl implements MetricService {

    private final MetricMapper metricMapper;

    @Override
    public List<Map> findById() {
        return null;
    }

    @Override
    public int save(MetricDto.Save data) {
        metricMapper.save(data);
        return 0;
    }



    @Override
    public List<Object> readFile() {
        String dir = "D:\\aaaa";
        try {
            Set<String> listFiles = listFilesUsingDirectoryStream(dir);
            System.out.println(listFiles.toString());
            for (String path : listFiles) {
                File file = new File(dir + "/" + path);
                try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                    String line;
                    HashMap<String, String[]> map = new HashMap<>();
                    //HashMap<String, String> lineMap = new HashMap<>();

                    int value = 1;
                    while ((line = br.readLine()) != null) {
                        System.out.println(line);
                        save(MetricDto.Save.builder()
                                .projectId("project_" + value)
                                .guId("gu_" +value)
                                .id(String.valueOf(value))
                                .info(line).build());
                        map.put("Value"+ value, line.split("\\s"));
                        System.out.println(Arrays.toString(line.split("\\s")));
                        value++;
                    }

                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    public Set<String> listFilesUsingDirectoryStream(String dir) throws IOException {
        Set<String> fileSet = new HashSet<>();
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(dir))) {
            for (Path path : stream) {
                if (!Files.isDirectory(path)) {
                    fileSet.add(path.getFileName()
                            .toString());
                }
            }
        }
        return fileSet;
    }
}
