package sko.ds.mlops.service;

import org.hibernate.mapping.Map;
import sko.ds.mlops.domain.MetricDto;

import java.util.List;

public interface MetricService {

    public List<Map> findById();
    public int save(MetricDto.Save data);
    public List<Object> readFile();

}
