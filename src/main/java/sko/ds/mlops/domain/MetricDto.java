package sko.ds.mlops.domain;


import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

public class MetricDto {

    @Data
    @Builder
    public static class Save {
        private String projectId;
        private String guId;
        private String id;
        private String info;
    }
}
