package sko.ds.mlops.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import sko.ds.mlops.domain.MetricDto;

@Mapper
public interface MetricMapper {

    @Insert("INSERT INTO TBL_METRIC (PJT_ID, GU_ID, ID, INFO) VALUES(#{save.projectId}, #{save.guId}, #{save.id}, #{save.info})")
    int save1(@Param("save") MetricDto.Save data);

    int save(MetricDto.Save data);
}
