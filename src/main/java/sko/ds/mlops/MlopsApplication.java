package sko.ds.mlops;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import sko.ds.mlops.dao.MetricMapper;
import sko.ds.mlops.service.serviceImpl.MetricServiceImpl;

@SpringBootApplication
public class MlopsApplication {

    public static void main(String[] args) {
        SpringApplication.run(MlopsApplication.class, args);
    }

}
